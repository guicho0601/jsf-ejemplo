package com.lrgt.hbm;

/**
 * Created by Luis on 10/07/17.
 */
public class Student {

    private int id, nota;
    private String name, aprobado;

    public Student() {}
    public Student(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() { return id; }
    public void setId(int id) { this.id = id; }
    public String getName() { return name; }
    public void setName(String name) { this.name = name; }
    public int getNota() { return nota; }
    public void setNota(int nota) { this.nota = nota; }
    public String getAprobado() { return nota >= 61 ? "Aprobado" : "Reprobado"; }

    @Override
    public Student clone() {
        return new Student(id, name);
    }

    public void restore(Student student) {
        this.id = student.getId();
        this.name = student.getName();
    }
}
