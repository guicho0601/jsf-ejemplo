package com.lrgt;

import com.lrgt.hbm.Student;
import com.lrgt.util.HibernateUtil;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Luis on 10/07/17.
 */
@ManagedBean
@SessionScoped
public class StudentCrudBean implements Serializable{

    private static final long serialVersionUID = 1L;
    private Student item = new Student();
    private Student beforeEditItem = null;

    private boolean edit, delete;

    public void add() {
        saveStudent();
    }

    private void saveStudent() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            if(isEdit() && !isDelete())
                session.update(item);
            else if(!isEdit() && !isDelete())
                session.save(item);
            else if(!isEdit() && isDelete())
                session.delete(item);
            tx.commit();
            item = new Student();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
                e.printStackTrace();
            }
        } finally {
            session.close();
        }
    }

    public void resetAdd() {
        item = new Student();
    }

    public void edit(Student item) {
        beforeEditItem = item.clone();
        this.item = item;
        edit = true;
    }

    public void cancelEdit() {
        this.item.restore(beforeEditItem);
        this.item = new Student();
        edit = false;
    }

    public void saveEdit() {
       saveStudent();
       edit = false;
    }

    public void delete(Student item) throws IOException {
        this.item = item;
        delete = true;
        saveStudent();
        delete = false;
    }

    public List getList() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        return session.createCriteria(Student.class).list();
    }

    public Student getItem() {
        return this.item;
    }

    public boolean isEdit() {
        return this.edit;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }
}
